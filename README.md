# Windows-Backup-Script
Custom backup script for Window server(s).

## Requirements
- Windows OS

## Setup

1) Download the files and export tem to any directory on Windows OS.

2) Change the list of directories/files you want this script to backup:
```Batch
set DIRS="W:\www" "C:\Games"
```

3) Change the location of the backups directory:
```Batch
set BACKUP_PLACE=P:\Backups
```

4) Perform the backup by simply double-clicking on the Backup.bat file or run this in DOS command prompt:
```Batch
call Backup.bat
```
