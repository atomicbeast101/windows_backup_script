@echo off
SetLocal EnableDelayedExpansion
:: # List of directories you want this script to backup.
set DIRS="W:\www" "C:\Games"

:: # Directory where you want all of the backups to be saved to (No slashes at end).
set BACKUP_PLACE=P:\Backups




:: Performs backup job. Best to leave the code below untouched unless you know Batch very well.
echo ==========[BACKUP]==========
echo  Starting backup...
set BACKUP_CMD=xcopy /s /c /d /e /h /i /r /y
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%"
set DATE_FILE=%MM%.%DD%.%YYYY%_%HH%.%Min%
(for %%f in (%DIRS%) do (
	set CUR_FILE=%%f
	set CUR_FILE=!CUR_FILE:"=!
	echo  Backing up !CUR_FILE!...
	set CUR_FILE_NM=!CUR_FILE!
	set CUR_FILE_NM=!CUR_FILE_NM::=.!
	set CUR_FILE_NM=!CUR_FILE_NM:\=[]!
	7za a -y -tzip "%BACKUP_PLACE%\%DATE_FILE%\!CUR_FILE_NM!.zip" "!CUR_FILE!" -mx5
	echo  Finished backing up !CUR_FILE!.
))
echo  Backup complete!
echo ==========[BACKUP]==========